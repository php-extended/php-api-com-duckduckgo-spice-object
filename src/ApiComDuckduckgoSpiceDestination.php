<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-duckduckgo-spice-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComDuckduckgoSpice;

/**
 * ApiComDuckduckgoSpiceDestination class file.
 * 
 * This is a simple implementation of the
 * ApiComDuckduckgoSpiceDestinationInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiComDuckduckgoSpiceDestination implements ApiComDuckduckgoSpiceDestinationInterface
{
	
	/**
	 * The destination currency code.
	 * 
	 * @var string
	 */
	protected string $_quotecurrency;
	
	/**
	 * The ratio.
	 * 
	 * @var float
	 */
	protected float $_mid;
	
	/**
	 * Constructor for ApiComDuckduckgoSpiceDestination with private members.
	 * 
	 * @param string $quotecurrency
	 * @param float $mid
	 */
	public function __construct(string $quotecurrency, float $mid)
	{
		$this->setQuotecurrency($quotecurrency);
		$this->setMid($mid);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the destination currency code.
	 * 
	 * @param string $quotecurrency
	 * @return ApiComDuckduckgoSpiceDestinationInterface
	 */
	public function setQuotecurrency(string $quotecurrency) : ApiComDuckduckgoSpiceDestinationInterface
	{
		$this->_quotecurrency = $quotecurrency;
		
		return $this;
	}
	
	/**
	 * Gets the destination currency code.
	 * 
	 * @return string
	 */
	public function getQuotecurrency() : string
	{
		return $this->_quotecurrency;
	}
	
	/**
	 * Sets the ratio.
	 * 
	 * @param float $mid
	 * @return ApiComDuckduckgoSpiceDestinationInterface
	 */
	public function setMid(float $mid) : ApiComDuckduckgoSpiceDestinationInterface
	{
		$this->_mid = $mid;
		
		return $this;
	}
	
	/**
	 * Gets the ratio.
	 * 
	 * @return float
	 */
	public function getMid() : float
	{
		return $this->_mid;
	}
	
}
