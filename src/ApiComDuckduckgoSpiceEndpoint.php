<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-duckduckgo-spice-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComDuckduckgoSpice;

use InvalidArgumentException;
use PhpExtended\DataProvider\JsonStringDataProvider;
use PhpExtended\DataProvider\UnprovidableJsonException;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Parser\ParseThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;

/**
 * ApiComDuckduckgoSpiceEndpoint class file.
 * 
 * This class is a simple implementation of the ApiComDuckduckgoSpiceEndpointInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiComDuckduckgoSpiceEndpoint implements ApiComDuckduckgoSpiceEndpointInterface
{
	
	public const HOST = 'https://duckduckgo.com/';
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 *
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The stream factory.
	 *
	 * @var StreamFactoryInterface
	 */
	protected StreamFactoryInterface $_streamFactory;
	
	/**
	 * The reifier.
	 *
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * Builds a new Endpoint with its dependancies.
	 *
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?StreamFactoryInterface $streamFactory
	 * @param ?ReifierInterface $reifier
	 */
	public function __construct(
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?StreamFactoryInterface $streamFactory = null,
		?ReifierInterface $reifier = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_streamFactory = $streamFactory ?? new StreamFactory();
		$this->_reifier = $reifier ?? new Reifier();
		
		$configuration = $this->_reifier->getConfiguration();
		$configuration->setIterableInnerTypes(ApiComDuckduckgoSpiceResponse::class, ['to'], ApiComDuckduckgoSpiceDestination::class);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiComDuckduckgoSpice\ApiComDuckduckgoSpiceEndpointInterface::getRates()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getRates(string $source, array $dests) : ApiComDuckduckgoSpiceResponseInterface
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'js/spice/currency/1/'.$source.'/'.\implode(',', $dests));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$data = $response->getBody()->__toString();
		
		$fpos = \mb_strpos($data, '{');
		if(false === $fpos)
		{
			// @codeCoverageIgnoreStart
			$message = 'Failed to decode json data : no "{chr}" found.';
			$context = ['{chr}' => '{'];
			
			throw new UnprovidableJsonException($uri->__toString(), 0, \strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		$rpos = \mb_strrpos($data, '}');
		if(false === $rpos)
		{
			// @codeCoverageIgnoreStart
			$message = 'Failed to decode json data : no "{chr}" found.';
			$context = ['{chr}' => '}'];
			
			throw new UnprovidableJsonException($uri->__toString(), 0, \strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		$data = (string) \mb_substr($data, $fpos, $rpos - $fpos + 1); // remove wrapper ddg_spice_currency( ... );
		
		return $this->_reifier->reify(ApiComDuckduckgoSpiceResponse::class, (new JsonStringDataProvider($data))->provideOne());
	}
	
}
