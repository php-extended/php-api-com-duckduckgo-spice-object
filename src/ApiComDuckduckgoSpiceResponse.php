<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-duckduckgo-spice-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComDuckduckgoSpice;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiComDuckduckgoSpiceResponse class file.
 * 
 * This is a simple implementation of the
 * ApiComDuckduckgoSpiceResponseInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiComDuckduckgoSpiceResponse implements ApiComDuckduckgoSpiceResponseInterface
{
	
	/**
	 * The link to the legal terms of use.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_terms;
	
	/**
	 * The link to the privacy policy.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_privacy;
	
	/**
	 * The source currency code.
	 * 
	 * @var string
	 */
	protected string $_from;
	
	/**
	 * The initial amount.
	 * 
	 * @var float
	 */
	protected float $_amount;
	
	/**
	 * The timestamp of the conversion.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_timestamp;
	
	/**
	 * The conversions.
	 * 
	 * @var array<int, ApiComDuckduckgoSpiceDestinationInterface>
	 */
	protected array $_to = [];
	
	/**
	 * Constructor for ApiComDuckduckgoSpiceResponse with private members.
	 * 
	 * @param UriInterface $terms
	 * @param UriInterface $privacy
	 * @param string $from
	 * @param float $amount
	 * @param DateTimeInterface $timestamp
	 * @param array<int, ApiComDuckduckgoSpiceDestinationInterface> $to
	 */
	public function __construct(UriInterface $terms, UriInterface $privacy, string $from, float $amount, DateTimeInterface $timestamp, array $to)
	{
		$this->setTerms($terms);
		$this->setPrivacy($privacy);
		$this->setFrom($from);
		$this->setAmount($amount);
		$this->setTimestamp($timestamp);
		$this->setTo($to);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the link to the legal terms of use.
	 * 
	 * @param UriInterface $terms
	 * @return ApiComDuckduckgoSpiceResponseInterface
	 */
	public function setTerms(UriInterface $terms) : ApiComDuckduckgoSpiceResponseInterface
	{
		$this->_terms = $terms;
		
		return $this;
	}
	
	/**
	 * Gets the link to the legal terms of use.
	 * 
	 * @return UriInterface
	 */
	public function getTerms() : UriInterface
	{
		return $this->_terms;
	}
	
	/**
	 * Sets the link to the privacy policy.
	 * 
	 * @param UriInterface $privacy
	 * @return ApiComDuckduckgoSpiceResponseInterface
	 */
	public function setPrivacy(UriInterface $privacy) : ApiComDuckduckgoSpiceResponseInterface
	{
		$this->_privacy = $privacy;
		
		return $this;
	}
	
	/**
	 * Gets the link to the privacy policy.
	 * 
	 * @return UriInterface
	 */
	public function getPrivacy() : UriInterface
	{
		return $this->_privacy;
	}
	
	/**
	 * Sets the source currency code.
	 * 
	 * @param string $from
	 * @return ApiComDuckduckgoSpiceResponseInterface
	 */
	public function setFrom(string $from) : ApiComDuckduckgoSpiceResponseInterface
	{
		$this->_from = $from;
		
		return $this;
	}
	
	/**
	 * Gets the source currency code.
	 * 
	 * @return string
	 */
	public function getFrom() : string
	{
		return $this->_from;
	}
	
	/**
	 * Sets the initial amount.
	 * 
	 * @param float $amount
	 * @return ApiComDuckduckgoSpiceResponseInterface
	 */
	public function setAmount(float $amount) : ApiComDuckduckgoSpiceResponseInterface
	{
		$this->_amount = $amount;
		
		return $this;
	}
	
	/**
	 * Gets the initial amount.
	 * 
	 * @return float
	 */
	public function getAmount() : float
	{
		return $this->_amount;
	}
	
	/**
	 * Sets the timestamp of the conversion.
	 * 
	 * @param DateTimeInterface $timestamp
	 * @return ApiComDuckduckgoSpiceResponseInterface
	 */
	public function setTimestamp(DateTimeInterface $timestamp) : ApiComDuckduckgoSpiceResponseInterface
	{
		$this->_timestamp = $timestamp;
		
		return $this;
	}
	
	/**
	 * Gets the timestamp of the conversion.
	 * 
	 * @return DateTimeInterface
	 */
	public function getTimestamp() : DateTimeInterface
	{
		return $this->_timestamp;
	}
	
	/**
	 * Sets the conversions.
	 * 
	 * @param array<int, ApiComDuckduckgoSpiceDestinationInterface> $to
	 * @return ApiComDuckduckgoSpiceResponseInterface
	 */
	public function setTo(array $to) : ApiComDuckduckgoSpiceResponseInterface
	{
		$this->_to = $to;
		
		return $this;
	}
	
	/**
	 * Gets the conversions.
	 * 
	 * @return array<int, ApiComDuckduckgoSpiceDestinationInterface>
	 */
	public function getTo() : array
	{
		return $this->_to;
	}
	
}
