<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-duckduckgo-spice-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComDuckduckgoSpice\Test;

use PhpExtended\ApiComDuckduckgoSpice\ApiComDuckduckgoSpiceDestination;
use PHPUnit\Framework\TestCase;

/**
 * ApiComDuckduckgoSpiceDestinationTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiComDuckduckgoSpice\ApiComDuckduckgoSpiceDestination
 * @internal
 * @small
 */
class ApiComDuckduckgoSpiceDestinationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiComDuckduckgoSpiceDestination
	 */
	protected ApiComDuckduckgoSpiceDestination $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetQuotecurrency() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getQuotecurrency());
		$expected = 'qsdfghjklm';
		$this->_object->setQuotecurrency($expected);
		$this->assertEquals($expected, $this->_object->getQuotecurrency());
	}
	
	public function testGetMid() : void
	{
		$this->assertEquals(14.1, $this->_object->getMid());
		$expected = 15.2;
		$this->_object->setMid($expected);
		$this->assertEquals($expected, $this->_object->getMid());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiComDuckduckgoSpiceDestination('azertyuiop', 14.1);
	}
	
}
