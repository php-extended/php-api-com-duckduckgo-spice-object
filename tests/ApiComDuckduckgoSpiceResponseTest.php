<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-duckduckgo-spice-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComDuckduckgoSpice\Test;

use DateTimeImmutable;
use PhpExtended\ApiComDuckduckgoSpice\ApiComDuckduckgoSpiceDestination;
use PhpExtended\ApiComDuckduckgoSpice\ApiComDuckduckgoSpiceResponse;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiComDuckduckgoSpiceResponseTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiComDuckduckgoSpice\ApiComDuckduckgoSpiceResponse
 * @internal
 * @small
 */
class ApiComDuckduckgoSpiceResponseTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiComDuckduckgoSpiceResponse
	 */
	protected ApiComDuckduckgoSpiceResponse $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetTerms() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getTerms());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setTerms($expected);
		$this->assertEquals($expected, $this->_object->getTerms());
	}
	
	public function testGetPrivacy() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getPrivacy());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setPrivacy($expected);
		$this->assertEquals($expected, $this->_object->getPrivacy());
	}
	
	public function testGetFrom() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getFrom());
		$expected = 'qsdfghjklm';
		$this->_object->setFrom($expected);
		$this->assertEquals($expected, $this->_object->getFrom());
	}
	
	public function testGetAmount() : void
	{
		$this->assertEquals(14.1, $this->_object->getAmount());
		$expected = 15.2;
		$this->_object->setAmount($expected);
		$this->assertEquals($expected, $this->_object->getAmount());
	}
	
	public function testGetTimestamp() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), $this->_object->getTimestamp());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setTimestamp($expected);
		$this->assertEquals($expected, $this->_object->getTimestamp());
	}
	
	public function testGetTo() : void
	{
		$this->assertEquals([$this->getMockBuilder(ApiComDuckduckgoSpiceDestination::class)->disableOriginalConstructor()->getMock()], $this->_object->getTo());
		$expected = [$this->getMockBuilder(ApiComDuckduckgoSpiceDestination::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiComDuckduckgoSpiceDestination::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setTo($expected);
		$this->assertEquals($expected, $this->_object->getTo());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiComDuckduckgoSpiceResponse((new UriParser())->parse('https://test.example.com'), (new UriParser())->parse('https://test.example.com'), 'azertyuiop', 14.1, DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), [$this->getMockBuilder(ApiComDuckduckgoSpiceDestination::class)->disableOriginalConstructor()->getMock()]);
	}
	
}
