<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-duckduckgo-spice-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiComDuckduckgoSpice\ApiComDuckduckgoSpiceEndpoint;
use PhpExtended\ApiComDuckduckgoSpice\ApiComDuckduckgoSpiceResponse;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\StringStream;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * ApiComDuckduckgoSpiceHeaders class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiComDuckduckgoSpice\ApiComDuckduckgoSpiceEndpoint
 * 
 * @internal
 *
 * @small
 */
class ApiComDuckduckgoSpiceEndpointTest extends TestCase
{
	
	/**
	 * The endpoint to test.
	 * 
	 * @var ApiComDuckduckgoSpiceEndpoint
	 */
	protected ApiComDuckduckgoSpiceEndpoint $_endpoint;
	
	public function testEurUsd() : void
	{
		$resp = $this->_endpoint->getRates('EUR', ['USD']);
		$this->assertInstanceOf(ApiComDuckduckgoSpiceResponse::class, $resp);
	}
	
	public function testKwdZwd() : void
	{
		$resp = $this->_endpoint->getRates('KWD', ['ZWD']);
		$this->assertInstanceOf(ApiComDuckduckgoSpiceResponse::class, $resp);
	}
	
	public function testGetAll() : void
	{
		$resp = $this->_endpoint->getRates('GBP', \explode(',', 'AED,AFN,ALL,AMD,ANG,AOA,ARS,AUD,AWG,AZN,BAM,BBD,BDT,BGN,BHD,BIF,BMD,BND,BOB,BRL,BSD,BTN,BWP,BYR,BZD,CAD,CDF,CHF,CLP,CNY,COP,CRC,CUC,CUP,CVE,CZK,DJF,DKK,DOP,DZD,EGP,ERN,ETB,EUR,FJD,FKP,GBP,GEL,GGP,GHS,GIP,GMD,GNF,GTQ,GYD,HKD,HNL,HRK,HTG,HUF,IDR,ILS,IMP,INR,IQD,IRR,ISK,JEP,JMD,JOD,JPY,KES,KGS,KHR,KMF,KPW,KRW,KWD,KYD,KZT,LAK,LBP,LKR,LRD,LSL,LYD,MAD,MDL,MGA,MKD,MMK,MNT,MOP,MRO,MUR,MVR,MWK,MXN,MYR,MZN,NAD,NGN,NIO,NOK,NPR,NZD,OMR,PAB,PEN,PGK,PHP,PKR,PLN,PYG,QAR,RON,RSD,RUB,RWF,SAR,SBD,SCR,SDG,SEK,SGD,SHP,SLL,SOS,SPL,SRD,STD,SVC,SYP,SZL,THB,TJS,TMT,TND,TOP,TRY,TTD,TVD,TWD,TZS,UAH,UGX,USD,UYU,UZS,VEF,VND,VUV,WST,XAF,XAG,XAU,XCD,XDR,XOF,XPD,XPF,XPT,YER,ZAR,ZMW,ZWD'));
		$this->assertInstanceOf(ApiComDuckduckgoSpiceResponse::class, $resp);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface
		{
			
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				$options = ['http' => ['user_agent' => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0']];
				$data = \file_get_contents($request->getUri()->__toString(), false, \stream_context_create($options));
				$body = new StringStream($data);
				
				return (new Response())->withBody($body);
			}
		};
		
		$this->_endpoint = new ApiComDuckduckgoSpiceEndpoint($client);
	}
	
}
